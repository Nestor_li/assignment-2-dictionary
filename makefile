SOURCE=$(wildcard *asm)
FLAGS=-felf64
TARGET=main
OBJ=$(SOURCE:.asm=.o)
LD=ld	

all: $(TARGET)

%.o: %.asm
	nasm $(FLAGS) $< -o $@

$(TARGET): $(OBJ)
	$(LD) $^ -o $@

clean:
	rm -fr *.o

