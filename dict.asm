section .text
%include "lib.inc"

global find_word
global print_dict


find_word:
    .while:
    	push rsi
    	add rsi, 8	; сл элемент
    	call string_equals ; проверяем, тот ли что мы искали
    	pop rsi 
    	test rax, rax ; рез проверочки
    	jne .found 
    	mov rsi, [rsi] 	; Переходим к следующему
    	test rax, rax 	; если сл элемента нет,
    	je .not_found ; то завершаем поиск
    	jmp .while ; else again
    .found:
        mov rax, rsi
        ret 
    .not_found:
        xor rax, rax
        ret	


