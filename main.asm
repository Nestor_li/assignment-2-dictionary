section .rodata
buffer_link: resb 256            ; Буффер на 256 символов
magical_num: 8


section .data
%include "words.inc"
%include "dict.inc"
%include "lib.inc"

launch_message: db "Input Key> ", 0
not_found_message: db "Key not found exeption", 10, 0
buffer_exeption_message: db "Buffer exeption", 10, 0


section .text
global _start

_start:                         ; TRANSLATE
    mov rdi, launch_message     ; String rdi = launch_message
    call print_string           ; cout << rdi   (launch_message)
    mov rdi, buffer_link        ; rdi = buffer (link)
    mov rsi, 256                ; rax = len(buffer)
    call read_word              ; read_word
    test rax, rax                  ; if (res == 0) // read not successful
    je .unreadable_exeption     ;   call unreadable exeption
    mov rdi, rax                ; rdi = key (read)
    mov rsi, link               ; rsi = next elem link 
    call find_word              ; find_word()
    test rax, rax                  ; if (not found):
    je .not_found_exeption      ;   call not_found_exeption
    mov rdi, rax                
    add rdi, magical_num                 
    push rdi                
    call string_length
    pop rdi
    lea rdi, [rdi + rax + 1]
    call print_string
    call print_newline
    xor rdi, rdi
    call exit
    
    .unreadable_exeption:
        mov rdi, buffer_exeption_message
        call print_string
        mov rdi, 2              ; TO std:error
        call exit
    
    .not_found_exeption:
        mov rdi, not_found_message
        call print_string
        mov rdi, 2              ; TO std:error
        call exit
        

